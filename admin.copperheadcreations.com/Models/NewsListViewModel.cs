﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class NewsListViewModel
    {
        public List<NewsModel> List { get; set; }
    }
}
