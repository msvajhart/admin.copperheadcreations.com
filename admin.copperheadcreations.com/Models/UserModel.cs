﻿namespace admin.copperheadcreations.com.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int? AccessId { get; set; }
        public int? CompanyId { get; set; }

    }
}