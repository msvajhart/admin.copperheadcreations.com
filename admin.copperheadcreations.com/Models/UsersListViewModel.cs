﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class UsersListViewModel
    {
        public List<UserModel> List { get; set; }
    }
}
