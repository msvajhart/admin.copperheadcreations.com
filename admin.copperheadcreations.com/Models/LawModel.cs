﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class LawModel
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string ShortDesc { get; set; }
        public string Description { get; set; }
    }
}
