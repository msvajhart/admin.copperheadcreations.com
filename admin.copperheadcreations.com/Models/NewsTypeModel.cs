﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class NewsTypeModel
    {
        public int Id { get; set; }
        public string TypeTag { get; set; }
        public string TypeTitle { get; set; }
    }
}
