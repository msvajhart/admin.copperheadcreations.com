﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class NewsEditViewModel
    {
        public bool Edit { get; set; }
        public List<NewsTypeModel> TypeList{ get; set; }
        public NewsModel News { get; set; }
    }
}
