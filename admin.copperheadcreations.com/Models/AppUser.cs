﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class AppUser
    {
        public NavModel Nav { get; set; }

        public SecurityModel Security { get; set; }

        public UserModel User { get; set; }

        public object ViewModel { get; set; }
    }
}
