﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class NewsModel
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Slug { get; set; }
        public bool Published { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
        public DateTime? Date { get; set; }
        public string PreviewText { get; set; }
        public int? TypeId { get; set; }

    }
}
