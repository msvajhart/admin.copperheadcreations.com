﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class SecurityModel
    {
        public bool SuperAdmin { get; set; }

        public bool SiteAdmin { get; set; }

        public bool ContentAdmin { get; set; }
    }
}
