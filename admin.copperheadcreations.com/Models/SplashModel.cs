﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace admin.copperheadcreations.com.Models
{
    public class SplashModel
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
