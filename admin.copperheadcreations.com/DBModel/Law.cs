﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Law
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string ShortDesc { get; set; }
        public string Description { get; set; }
    }
}
