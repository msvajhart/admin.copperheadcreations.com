﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Team
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string Slug { get; set; }
        public string ImageName { get; set; }
        public string FocuspointData { get; set; }
        public string Bio { get; set; }
        public string Role { get; set; }
    }
}
