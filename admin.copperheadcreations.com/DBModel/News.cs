﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class News
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Slug { get; set; }
        public sbyte Published { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public DateTime? Date { get; set; }
        public string PreviewText { get; set; }
        public int? TypeId { get; set; }
    }
}
