﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class LawChildrenDb
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public int LawId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
