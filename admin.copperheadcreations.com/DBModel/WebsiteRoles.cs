﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class WebsiteRoles
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Name { get; set; }
    }
}
