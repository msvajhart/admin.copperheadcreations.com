﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Websites
    {
        public int Id { get; set; }
        public string Domain { get; set; }
        public int CompanyId { get; set; }
    }
}
