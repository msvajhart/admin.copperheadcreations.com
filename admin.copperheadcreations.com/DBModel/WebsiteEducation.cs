﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class WebsiteEducation
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string University { get; set; }
        public string Extra { get; set; }
    }
}
