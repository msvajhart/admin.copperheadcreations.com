﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Splash
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
