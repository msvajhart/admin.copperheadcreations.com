﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class TeamExtras
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public int TeamId { get; set; }
        public int Category { get; set; }
        public string Data { get; set; }
    }
}
