﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class TeamEducation
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public int WebsiteEducationId { get; set; }
    }
}
