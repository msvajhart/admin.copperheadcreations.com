﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Sessions
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string SessionId { get; set; }
        public DateTime LastActive { get; set; }
        public sbyte Active { get; set; }
    }
}
