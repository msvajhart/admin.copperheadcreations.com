﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Leads
    {
        public int Id { get; set; }
        public int? WebsiteId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Comments { get; set; }
    }
}
