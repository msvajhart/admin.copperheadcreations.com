﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class UserAccess
    {
        public int Id { get; set; }
        public string RoleType { get; set; }
    }
}
