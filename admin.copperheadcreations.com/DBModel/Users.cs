﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Users
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public int? AccessId { get; set; }
        public int? CompanyId { get; set; }
    }
}
