﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
