﻿using System;
using System.Collections.Generic;

namespace admin.copperheadcreations.com
{
    public partial class NewsType
    {
        public int Id { get; set; }
        public int WebsiteId { get; set; }
        public string TypeTag { get; set; }
        public string TypeTitle { get; set; }
    }
}
