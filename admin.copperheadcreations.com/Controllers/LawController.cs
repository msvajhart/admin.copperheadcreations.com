﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using admin.copperheadcreations.com.Logic;
using admin.copperheadcreations.com.Models;
using Microsoft.AspNetCore.Mvc;

namespace admin.copperheadcreations.com.Controllers
{
    public class LawController : Controller
    {
        private readonly dbcopperheadContext _context;

        public LawController(dbcopperheadContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            LawViewModel Model = new LawViewModel();
                
            List<LawModel> List = new List<LawModel>();


            var query = _context.Law.Where(a => a.WebsiteId == 1);

            foreach (var row in query)
            {
                LawModel Law = new LawModel
                {
                    Id = row.Id,
                    WebsiteId = row.WebsiteId,
                    Title = row.Title,
                    Slug = row.Slug,
                    Icon = row.Icon,
                    ShortDesc = row.ShortDesc,
                    Description = row.Description,
                };
                List.Add(Law);
            }

            Model.List = List;

            return View(Model);
        }

        public IActionResult Create()
        {

            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            LawModel Model = new LawModel();

            Model.WebsiteId = 1;

            return View(Model);
        }

        public IActionResult Edit(int id)
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }


            var query = _context.Law.Where(a => a.Id == id).SingleOrDefault();

            if (query == null)
            {
                return RedirectToAction("Index");
            }

            LawModel Law = new LawModel
            {
                Id = query.Id,
                WebsiteId = query.WebsiteId,
                Title = query.Title,
                Slug = query.Slug,
                Icon = query.Icon,
                ShortDesc = query.ShortDesc,
                Description = query.Description,
            };
            return View(Law);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateLaw(LawModel law)
        {

            using (var context = new dbcopperheadContext())
            {
                var newlaw = new Law()
                {
                    Title = law.Title,
                    WebsiteId = law.WebsiteId,
                    Slug = law.Slug,
                    Icon = law.Icon,
                    ShortDesc = law.ShortDesc,
                    Description = law.Description
                };
                context.Law.Add(newlaw);
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditLaw(LawModel law)
        {
            using (var context = new dbcopperheadContext())
            {

                var newlaw = new Law()
                {
                    Id = law.Id,
                    Title = law.Title,
                    WebsiteId = law.WebsiteId,
                    Slug = law.Slug,
                    Icon = law.Icon,
                    ShortDesc = law.ShortDesc,
                    Description = law.Description
                };
                context.Law.Update(newlaw);
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

    }
}