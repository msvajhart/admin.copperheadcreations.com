﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using admin.copperheadcreations.com.Models;
using admin.copperheadcreations.com.FilterAttributes;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Text;
using admin.copperheadcreations.com.Logic;
using Microsoft.AspNetCore.Http;

namespace admin.copperheadcreations.com.Controllers
{
    public class HomeController : Controller
    {
        private readonly dbcopperheadContext _context;

        public HomeController(dbcopperheadContext context)
        {
            _context = context;
        }

        /// <summary>
        /// login page
        /// </summary>
        /// <returns></returns>
        [ViewLayout("_CleanLayout")]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Home Page Dashboard
        /// </summary>
        /// <returns></returns>
        public IActionResult Dashboard()
        {
            if (!Authenticate.Check(_context, HttpContext)) {
                return RedirectToAction("Logout", "Home");
            }
            return View();
        }

        public IActionResult Logout()
        {
            string session = HttpContext.Session.GetString("session");
            var query = _context.Sessions.Where(x => x.SessionId == session).FirstOrDefault();

            if (query != null)
            {
                using (var context = new dbcopperheadContext())
                {
                    var logout = new Sessions()
                    {
                        Id = query.Id,
                        Active = 0,
                        LastActive = DateTime.Now,
                        UserId = query.UserId,
                        SessionId = query.SessionId,

                    };

                    context.Sessions.Update(logout);
                    context.SaveChanges();
                }
            }

            HttpContext.Session.Clear();

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Method Validates Login
        /// </summary>
        /// <param name="User">Form LoginModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginModel User)
        {

            var query = _context.Users.Where(x => x.Username == User.Username).FirstOrDefault();

            if(query != null)
            {
                    if (Authenticate.CheckLogin(User.Password, query.Salt, query.Password))
                    {
                        Authenticate.CreateSession(query.Id, _context, HttpContext);
                        string confirm = HttpContext.Session.GetString("session");
                        return RedirectToAction("Dashboard");
                    }
            }
            //validate with api
            
            return RedirectToAction("", new { id = 1 });
        }

        


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
