﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using admin.copperheadcreations.com.Logic;
using admin.copperheadcreations.com.Models;
using Microsoft.AspNetCore.Mvc;

namespace admin.copperheadcreations.com.Controllers
{
    public class SplashController : Controller
    {
        private readonly dbcopperheadContext _context;

        public SplashController(dbcopperheadContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }
            SplashViewModel Model = new SplashViewModel();

            List<SplashModel> list = new List<SplashModel>();

            var query = _context.Splash.Where(a => a.WebsiteId == 1);

            foreach (var row in query)
            {
                SplashModel SplashBlock = new SplashModel {
                    Id = row.Id,
                    WebsiteId = row.WebsiteId,
                    Title = row.Title,
                    Text = row.Text
                };

                list.Add(SplashBlock);
            }
            Model.List = list;
            return View(Model);
        }


        public IActionResult Create()
        {

            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            SplashModel Model = new SplashModel();

            Model.WebsiteId = 1;

            return View(Model);
        }

        public IActionResult Edit(int id)
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }


            var query = _context.Splash.Where(a => a.Id == id).SingleOrDefault();

            if (query == null)
            {
                return RedirectToAction("Index");
            }

            SplashModel Model = new SplashModel
            {
                Id = query.Id,
                WebsiteId = query.WebsiteId,
                Title = query.Title,
                Text = query.Text
            };
            return View(Model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateSplash(SplashModel splash)
        {

            using (var context = new dbcopperheadContext())
            {
                var box = new Splash()
                {
                    Title = splash.Title,
                    WebsiteId = splash.WebsiteId,
                    Text = splash.Text
                };
                context.Splash.Add(box);
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditSplash(SplashModel splash)
        {
            using (var context = new dbcopperheadContext())
            {

                var box = new Splash()
                {
                    Id = splash.Id,
                    Title = splash.Title,
                    WebsiteId = splash.WebsiteId,
                    Text = splash.Text
                };
                context.Splash.Update(box);
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            var box = _context.Splash.Where(b => b.Id == id).FirstOrDefault();
            if (box != null)
            {
                using (var context = new dbcopperheadContext())
                {
                    context.Splash.Remove(box);
                    context.SaveChanges();
                }
            }


            return RedirectToAction("Index");
        }

    }
}