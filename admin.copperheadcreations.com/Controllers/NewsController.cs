﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using admin.copperheadcreations.com.Logic;
using admin.copperheadcreations.com.Models;
using Microsoft.AspNetCore.Mvc;

namespace admin.copperheadcreations.com.Controllers
{
    public class NewsController : Controller
    {

        private readonly dbcopperheadContext _context;

        public NewsController(dbcopperheadContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            NewsListViewModel model = new NewsListViewModel();

            List<NewsModel> List = new List<NewsModel>();


            var query = _context.News.Where(a => a.WebsiteId == 1).OrderByDescending(a => a.Date);

            foreach (var row in query)
            {
                NewsModel Article = new NewsModel
                {
                    Id = row.Id,
                    Slug = row.Slug,
                    Published = Convert.ToBoolean(row.Published),
                    Title = row.Title,
                    Content = row.Content,
                    Author = row.Author,
                    Date = row.Date,
                    PreviewText = row.PreviewText,
                    TypeId = row.TypeId
                };
                List.Add(Article);
            }

            model.List = List;
            return View(model);
        }

        public IActionResult Create()
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            List<NewsTypeModel> List = new List<NewsTypeModel>();
            var query = _context.NewsType.Where(a => a.WebsiteId == 1).OrderByDescending(a => a.Id);
            foreach (var row in query)
            {
                NewsTypeModel NewsType = new NewsTypeModel
                {
                    Id = row.Id,
                    TypeTag = row.TypeTag,
                    TypeTitle = row.TypeTitle
                };
                List.Add(NewsType);
            }

            NewsModel Article = new NewsModel
            {
                WebsiteId = 1,
            };

            NewsEditViewModel Model = new NewsEditViewModel {
                Edit = false,
                TypeList = List,
                News = Article
            };


            return View(Model);
        }

        public IActionResult Edit(int id)
        {

            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            List<NewsTypeModel> List = new List<NewsTypeModel>();
            var newstype_query = _context.NewsType.Where(a => a.WebsiteId == 1).OrderByDescending(a => a.Id);

            

            foreach (var row in newstype_query)
            {
                NewsTypeModel NewsType = new NewsTypeModel
                {
                    Id = row.Id,
                    TypeTag = row.TypeTag,
                    TypeTitle = row.TypeTitle
                };
                List.Add(NewsType);
            }

            var article_query = _context.News.Where(a => a.WebsiteId == 1 && a.Id == id).FirstOrDefault();

            if(article_query == null)
            {
                return RedirectToAction("Index");
            }

            NewsModel Article = new NewsModel
            {
                Id = article_query.Id,
                WebsiteId = article_query.WebsiteId,
                Slug = article_query.Slug,
                Published = Convert.ToBoolean(article_query.Published),
                Title = article_query.Title,
                Content = article_query.Content,
                Author = article_query.Author,
                Date = article_query.Date,
                PreviewText = article_query.PreviewText,
                TypeId = article_query.TypeId
            };

            NewsEditViewModel Model = new NewsEditViewModel
            {
                Edit = false,
                TypeList = List,
                News = Article
            };

            return View(Model);
        }

        public IActionResult Delete(int id)
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            var Article = _context.News.Where(b => b.Id == id).FirstOrDefault();
            if (Article != null)
            {
                _context.News.Remove(Article);
                _context.SaveChanges();
            }


            return RedirectToAction("Index");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreatePost(NewsEditViewModel article)
        {
            using (var context = new dbcopperheadContext())
            {
                string author;
                if (article.News.Author == null)
                {
                    author = "";
                }
                else
                {
                    author = article.News.Author;
                }

                var newpost = new News()
                {
                    Title = article.News.Title,
                    WebsiteId = article.News.WebsiteId,
                    Slug = article.News.Slug,
                    Author = author,
                    Published = Convert.ToSByte(article.News.Published),
                    Date = article.News.Date,
                    PreviewText = article.News.PreviewText,
                    Content = article.News.Content,
                    TypeId = article.News.TypeId
                };
                context.News.Add(newpost);
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPost(NewsEditViewModel article)
        {
            using (var context = new dbcopperheadContext())
            {

                string author;
                if (article.News.Author == null)
                {
                    author = "";
                }
                else
                {
                    author = article.News.Author;
                }
                var editPost = new News()
                {
                    Id = article.News.Id,
                    Title = article.News.Title,
                    WebsiteId = article.News.WebsiteId,
                    Slug = article.News.Slug,
                    Author = author,
                    Published = Convert.ToSByte(article.News.Published),
                    Date = article.News.Date,
                    PreviewText = article.News.PreviewText,
                    Content = article.News.Content,
                    TypeId = article.News.TypeId
                };
                context.News.Update(editPost);
                context.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}