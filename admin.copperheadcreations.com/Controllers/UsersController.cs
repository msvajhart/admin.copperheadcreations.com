﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using admin.copperheadcreations.com.Logic;
using admin.copperheadcreations.com.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;

namespace admin.copperheadcreations.com.Controllers
{
    public class UsersController : Controller
    {
        private readonly dbcopperheadContext _context;

        public UsersController(dbcopperheadContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            if(!Authenticate.Security(_context, HttpContext, 1))
            {
                return RedirectToAction("Dashboard", "Home");
            }


            UsersListViewModel model = new UsersListViewModel();

            List<UserModel> List = new List<UserModel>();


            var query = _context.Users.ToList();

            foreach(var row in query)
            {
                UserModel User = new UserModel
                {
                    Id = row.Id,
                    Username = row.Username,
                    AccessId = row.AccessId,
                    CompanyId = row.CompanyId
                };
                List.Add(User);
            }

            model.List = List;
            return View(model);
        }

        /// <summary>
        /// Create user page
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            if (!Authenticate.Security(_context, HttpContext, 1))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            return View();
        }

        /// <summary>
        /// Create user page
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit(string id)
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            if (!Authenticate.Security(_context, HttpContext, 1))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            return View();
        }

        /// <summary>
        /// Create user page
        /// </summary>
        /// <returns></returns>
        public IActionResult Delete(int id)
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            if (!Authenticate.Security(_context, HttpContext, 1))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            var User = _context.Users.Where(b => b.Id == id).FirstOrDefault();
            if(User != null)
            {
                _context.Users.Remove(User);
                _context.SaveChanges();
            }
            

            return RedirectToAction("Index");
        }


        /// <summary>
        /// Edit user page
        /// </summary>
        /// <returns></returns>
        public IActionResult Summary()
        {
            if (!Authenticate.Check(_context, HttpContext))
            {
                return RedirectToAction("Logout", "Home");
            }

            if (!Authenticate.Security(_context, HttpContext, 1))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            return View();
        }


        /// <summary>
        /// Method Creates a new user.
        /// </summary>
        /// <param name="User">Form CreateUserModel</param>
        /// <returns>redirect action to dashboard</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateUser(CreateUserModel User)
        {

            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            string saltbase64 = Convert.ToBase64String(salt);

            string hashedPassword = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: User.Password,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

            bool canLogin = Authenticate.CheckLogin(User.Password, saltbase64, hashedPassword);

            if (canLogin)
            {
                using (var context = new dbcopperheadContext())
                {
                    var newuser = new Users()
                    {
                        Username = User.Username,
                        Password = hashedPassword,
                        Salt = saltbase64,
                        AccessId = null,
                        CompanyId = null

                    };
                    context.Users.Add(newuser);
                    context.SaveChanges();
                }
            }

            return RedirectToAction("Index"); 
        }
    }
}