﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using admin.copperheadcreations.com.Models;
using Microsoft.AspNetCore.Mvc;

namespace admin.copperheadcreations.com.Controllers
{
    public class PreviewController : Controller
    {
        private readonly dbcopperheadContext _context;

        public PreviewController(dbcopperheadContext context)
        {
            _context = context;
        }

        public IActionResult Article(string id)
        {

            var n = _context.News.Where(i => i.Slug.ToLower() == id.ToLower() && i.WebsiteId == 1 ).FirstOrDefault();

            NewsModel Model = new NewsModel
            {
                Id = n.Id,
                Slug = n.Slug,
                Published = Convert.ToBoolean(n.Published),
                Title = n.Title,
                Content = n.Content,
                Author = n.Author,
                Date = n.Date,
                PreviewText = n.PreviewText,
                TypeId = n.TypeId
            };
            return View(Model);
        }

    }
}