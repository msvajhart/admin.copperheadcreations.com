﻿

function initNav() {
    $("body").on("click", ".menu__header", function () {
        $(this).parent().toggleClass("active");
    });

    $("body").on("click", ".nav__button", function () {
        $(".flexed__layout").toggleClass("active");
    });
}

function initquill() {
    
    var modQuill = {
        init: function () {
            var quill = new Quill('#editor', {
                theme: 'snow'
            });


            quill.on('text-change', function (delta, oldDelta, source) {
                if (source == 'api') {
                    console.log("An API call triggered this change.");
                    let content = $("#editor .ql-editor").html();
                    $("#Editor-Content").val(content);
                } else if (source == 'user') {
                    console.log("A user action triggered this change.");
                    let content = $("#editor .ql-editor").html();
                    $("#Editor-Content").val(content);
                }
            });
        }
    }

    if ($("#editor").length > 0) {
        modQuill.init();
    }

    
}

function initNewsSection() {
    $("body").on("change", "#Title > input", function () {
        console.log($(this).val())
        $('#Slug > input').val($(this).val());
        News.slugify("#Slug > input");
    });

    $("body").on("change", "#Slug > input", function () {
        News.slugify("#Slug > input");
    });

    var News = {
        slugify: function (target) {
            $this = $(target);
            let text = $this.val();


            text = News.string_to_slug(text);


            $this.val(text);
        },
        string_to_slug: function(str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to = "aaaaeeeeiiiioooouuuunc------";
            for (var i = 0, l = from.length; i < l; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

            return str;
        }

    }
}




$(function () {
    initNav();
    initquill();
    initNewsSection();
});
