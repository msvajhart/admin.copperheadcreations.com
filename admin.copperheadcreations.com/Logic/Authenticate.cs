﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace admin.copperheadcreations.com.Logic
{
    public class Authenticate
    {
        private static readonly HttpClient client = new HttpClient();

        /// <summary>
        /// Method full authentication process to confirm user still has access
        /// </summary>
        /// <returns> True if the user is still active false if the user is inactive or unfound</returns>
        public static bool Check(dbcopperheadContext _context, HttpContext httpContext)
        {
            string session = httpContext.Session.GetString("session");
            if(session != null) {
                var query = _context.Sessions.Where(x => x.SessionId == session).FirstOrDefault();
                if (query != null)
                {
                    if(query.Active == 1)
                    {
                        DateTime Now = DateTime.Now;
                        DateTime LastActive = query.LastActive;

                        TimeSpan Diff = Now.Subtract(LastActive);

                        if (Diff.Minutes <  20)
                        {
                            return true;
                        }
                    }
                }
            }
            
            return false;

        }

        public static bool Security(dbcopperheadContext _context, HttpContext httpContext, int minimum)
        {
            string session = httpContext.Session.GetString("session");
            if (session != null)
            {
                using (dbcopperheadContext context = new dbcopperheadContext())
                {
                    var query = (from u in context.Users
                                join s in context.Sessions
                                on u.Id equals s.UserId
                                where s.SessionId == session
                                select new
                                {
                                    Id = u.Id,
                                    UserAccess = u.AccessId,
                                    Session = s.SessionId
                                }).ToList();
                    foreach(var u in query)
                    {
                        if (u.UserAccess <= minimum)
                        {
                            return true;
                        }
                    }
                }
                
            }
            return false;
        }

        /// <summary>
        /// Method Creates a new session variable and saves the session to the DB
        /// </summary>
        /// <param name="id"></param>
        public static void CreateSession(int id, dbcopperheadContext _context, HttpContext httpContext)
        {

            string SessionString = randStr(new Random().Next(20, 25));
            using (var context = new dbcopperheadContext())
            {
                var newSession = new Sessions()
                {
                    Active = 1,
                    UserId = id,
                    SessionId = SessionString,
                    LastActive = DateTime.Now
                };

                context.Sessions.Add(newSession);
                context.SaveChanges();
            }

            httpContext.Session.SetString("session", SessionString);
            
        }

        

        public static string randStr(int size)
        {
            Random rand = new Random((int)DateTime.Now.Ticks);
            string input = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Range(0, size).Select(x => input[rand.Next(0, input.Length)]).ToArray());
        }

        /// <summary>
        /// Method finds session id and confirms it is still active
        /// </summary>
        /// <returns>True if session was found and is still active false if the session is inactive</returns>
        public static bool CheckSession()
        {
            return false;
        }

        /// <summary>
        /// Method takes session and sets current time
        /// </summary>
        /// <returns></returns>
        public static bool ExtendSession()
        {
            return true;
        }






        /// <summary>
        /// Confirms password with hash in DB
        /// </summary>
        /// <param name="inputPassword">Password from form</param>
        /// <param name="salt">Salt saved in DB</param>
        /// <param name="hashedPassword">Password saved in DB</param>
        /// <returns>True if passwords match False if passwords do not match</returns>
        public static bool CheckLogin(string inputPassword, string salt, string hashedPassword)
        {
            byte[] byteSalt = new byte[128 / 8];

            byteSalt = Convert.FromBase64String(salt);

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: inputPassword,
            salt: byteSalt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

            if (hashed == hashedPassword) return true;

            return false;
        }
    }
}
